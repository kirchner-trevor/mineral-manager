package me.hellfire212.mineralmanager;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.material.MaterialData;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

import java.util.List;

/**
 * Created by kirch on 11/5/2016.
 */
public class BlockBreakListener implements Listener {
    private Plugin _plugin;
    private Material _replaceWith;

    public BlockBreakListener(Plugin plugin, Material replaceWith) {
        _plugin = plugin;
        _replaceWith = replaceWith;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void OnBlockBreakEvent(BlockBreakEvent event) {
        event.getPlayer().sendMessage(event.getBlock().getState().getType().name() + " " + event.getBlock().getState().getData().toString());

        if (IsPlaceholder(event.getBlock())) {
            event.setCancelled(true);
            return;
        }

        if (!event.isCancelled()) {
            Material restoreMaterial = event.getBlock().getState().getType();
            MaterialData restoreMaterialData = event.getBlock().getState().getData();

            SetPlaceholderBlockTask setPlaceholderBlockTask = new SetPlaceholderBlockTask(
                    _plugin, event.getBlock(), _replaceWith, restoreMaterial, restoreMaterialData);

            Bukkit.getScheduler().runTask(_plugin, setPlaceholderBlockTask);
        }
    }

    public boolean IsPlaceholder(Block block) {
        boolean isPlaceholder = false;
        List<MetadataValue> metadata = block.getMetadata(MetadataKey.PLACEHOLDER);
        for (MetadataValue metadataValue : metadata) {
            if (metadataValue.getOwningPlugin() == _plugin) {
                isPlaceholder = metadataValue.asBoolean();
                break;
            }
        }
        return isPlaceholder;
    }
}
