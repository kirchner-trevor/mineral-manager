package me.hellfire212.mineralmanager;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.material.MaterialData;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

/**
 * Created by kirch on 11/6/2016.
 */
public class SetPlaceholderBlockTask implements Runnable {
    private Plugin _plugin;
    private Block _block;
    private Material _placeholderMaterial;
    private Material _restoreMaterial;
    private MaterialData _restoreMaterialData;

    public SetPlaceholderBlockTask(Plugin plugin, Block block, Material placeholderMaterial, Material restoreMaterial, MaterialData restoreMaterialData) {
        _plugin = plugin;
        _block = block;
        _placeholderMaterial = placeholderMaterial;
        _restoreMaterial = restoreMaterial;
        _restoreMaterialData = restoreMaterialData;
    }

    @Override
    public void run() {
        _block.setMetadata(MetadataKey.PLACEHOLDER, new FixedMetadataValue(_plugin, true));

        // TODO : Properly handle multi-blocks (tall grass / beds / ect)
        if (!TryHandleDoublePlant(_block, _restoreMaterialData)) {
            BlockState blockState = _block.getState();
            blockState.setType(_placeholderMaterial);
            blockState.update(true, true);
        }

        RestoreBlockEntity restoreBlockEntity = new RestoreBlockEntity();
        restoreBlockEntity.setLocation(_block.getLocation());
        restoreBlockEntity.setMaterialData(_restoreMaterialData);
        _plugin.getDatabase().save(restoreBlockEntity);

        RestoreBlockTask restoreBlockTask = new RestoreBlockTask(
                _plugin, restoreBlockEntity.getId(), _restoreMaterial, _restoreMaterialData);

        Bukkit.getScheduler().scheduleSyncDelayedTask(_plugin, restoreBlockTask, 100);
    }

    public boolean TryHandleDoublePlant(Block block, MaterialData materialData) {
        boolean didHandle = false;
        if (materialData.getItemType() == Material.DOUBLE_PLANT) {
            BlockState doublePlantBottom;
            BlockState doublePlantTop;

            if (materialData.getData() == (byte) 2) {
                doublePlantBottom = block.getState();
                doublePlantTop = block.getRelative(BlockFace.UP).getState();
            } else if (materialData.getData() == (byte) 10) {
                doublePlantBottom = block.getRelative(BlockFace.DOWN).getState();
                doublePlantTop = block.getState();
            } else {
                System.out.println("Invalid data for DOUBLE_PLANT");
                doublePlantBottom = null;
                doublePlantTop = null;
            }

            doublePlantBottom.setType(_placeholderMaterial);
            doublePlantTop.setType(_placeholderMaterial);
            doublePlantBottom.update(true, false);
            doublePlantTop.update(true, true);

            didHandle = true;
        }
        return didHandle;
    }
}