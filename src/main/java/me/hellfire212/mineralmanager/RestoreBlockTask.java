package me.hellfire212.mineralmanager;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.Plugin;

/**
 * Created by kirch on 11/6/2016.
 */
public class RestoreBlockTask implements Runnable {
    private Plugin _plugin;
    private int _restoreBlockEntityId;
    private Material _material;
    private MaterialData _materialData;

    public RestoreBlockTask(Plugin plugin, int restoreBlockEntityId, Material material, MaterialData materialData) {
        _plugin = plugin;
        _restoreBlockEntityId = restoreBlockEntityId;
        _material = material;
        _materialData = materialData;
    }

    @Override
    public void run() {
        RestoreBlockEntity restoreBlockEntity = _plugin.getDatabase().find(RestoreBlockEntity.class).where().idEq(_restoreBlockEntityId).findUnique();

        System.out.println("Attempting to restore " + restoreBlockEntity.toString());

        Block block = restoreBlockEntity.getLocation().getBlock();
        block.removeMetadata(MetadataKey.PLACEHOLDER, _plugin);

        // TODO : Properly handle multi-blocks (tall grass / beds / ect)
        if (!TryHandleDoublePlant(block, restoreBlockEntity.getMaterialData())) {
            BlockState blockState = block.getState();
            blockState.setType(restoreBlockEntity.getMaterialData().getItemType());
            blockState.setRawData(restoreBlockEntity.getMaterialData().getData());
            blockState.update(true, true);
        }

        _plugin.getDatabase().delete(restoreBlockEntity);
    }

    public boolean TryHandleDoublePlant(Block block, MaterialData materialData) {
        boolean didHandle = false;
        if (materialData.getItemType() == Material.DOUBLE_PLANT) {
            BlockState doublePlantBottom;
            BlockState doublePlantTop;

            if (materialData.getData() == (byte) 2) {
                doublePlantBottom = block.getState();
                doublePlantTop = block.getRelative(BlockFace.UP).getState();
            } else if (materialData.getData() == (byte) 10) {
                doublePlantBottom = block.getRelative(BlockFace.DOWN).getState();
                doublePlantTop = block.getState();
            } else {
                System.out.println("Invalid data for DOUBLE_PLANT");
                doublePlantBottom = null;
                doublePlantTop = null;
            }

            doublePlantBottom.setType(Material.DOUBLE_PLANT);
            doublePlantTop.setType(Material.DOUBLE_PLANT);
            doublePlantBottom.setRawData((byte) 2);
            doublePlantTop.setRawData((byte) 10);
            doublePlantBottom.update(true, false);
            doublePlantTop.update(true, true);

            didHandle = true;
        }
        return didHandle;
    }
}
