package me.hellfire212.mineralmanager;

import org.bukkit.Material;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kirch on 11/5/2016.
 */
public class MineralManager extends JavaPlugin {
    private Listener _blockBreakListener = new BlockBreakListener(this, Material.GLOWSTONE);

    @Override
    public void onEnable() {
        super.onEnable();

        setupListeners();
        setupDatabase();
    }

    @Override
    public void onDisable() {
        super.onDisable();

        cleanupListeners();
    }

    @Override
    public List<Class<?>> getDatabaseClasses() {
        List<Class<?>> list = new ArrayList<Class<?>>();
        list.add(RestoreBlockEntity.class);
        return list;
    }

    private void setupListeners() {
        getServer().getPluginManager().registerEvents(_blockBreakListener, this);
    }

    private void cleanupListeners() {
        HandlerList.unregisterAll(_blockBreakListener);
    }

    private void setupDatabase() {
        try {
            getDatabase().find(RestoreBlockEntity.class).findRowCount();
        } catch (PersistenceException ex) {
            System.out.println("[" + getDescription().getName() + "] Installing database...");
            installDDL();
        }
    }
}
