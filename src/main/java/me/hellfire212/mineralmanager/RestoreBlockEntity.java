package me.hellfire212.mineralmanager;

import com.avaje.ebean.validation.NotNull;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.material.MaterialData;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Created by kirch on 11/6/2016.
 */
@Entity
@Table(name = "RestoreBlockEntity")
public class RestoreBlockEntity {
    @Id
    private int id;

    @NotNull
    private UUID worldUID;
    @NotNull
    private double x;
    @NotNull
    private double y;
    @NotNull
    private double z;
    @NotNull
    private float pitch;
    @NotNull
    private float yaw;
    @NotNull
    private int type;
    @NotNull
    private byte data;

    @Override
    public String toString() {
        return "RestoreBlockEntity(" + id + ", " + type + ", " + data + ", " + worldUID + ", " + x + ", " + y + ", " + z + ")";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getWorldUID() {
        return worldUID;
    }

    public void setWorldUID(UUID worldUID) {
        this.worldUID = worldUID;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public byte getData() {
        return data;
    }

    public void setData(byte data) {
        this.data = data;
    }

    public MaterialData getMaterialData() {
        return new MaterialData(type, data);
    }

    public void setMaterialData(MaterialData materialData) {
        this.type = materialData.getItemTypeId();
        this.data = materialData.getData();
    }

    public Location getLocation() {
        World world = Bukkit.getServer().getWorld(worldUID);
        return new Location(world, x, y, z, yaw, pitch);
    }

    public void setLocation(Location location) {
        this.worldUID = location.getWorld().getUID();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
    }
}
